package com.example.kotlinexer.security;

import net.sf.ehcache.Element;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Ehcache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import java.io.IOException;

public class TicketAuthenticationAjaxRequestFilter implements Filter {

    private final static Logger LOGGER = LoggerFactory.getLogger(TicketAuthenticationAjaxRequestFilter.class);

    public static final String INVALID_TICKET = "INVALID_TICKET";

    private String excludePaths = "/public/list";
    private Ehcache cache;
    private String[] excludePathArrays;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        if (! ObjectUtils.isEmpty(excludePaths)) {
            excludePathArrays = excludePaths.trim().split(",");
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain)
            throws IOException, ServletException {

        LOGGER.info("---- TicketAuthenticationAjaxRequestFilter doFilter");

        if (excludePathArrays == null) {
            this.init(null);
        }
        final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        final HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        String uri = httpServletRequest.getRequestURI();
LOGGER.info("---- ajax doFilter uri: " + uri);
        String requestType = httpServletRequest.getHeader("X-Requested-With");
LOGGER.info("---- requestType: " + requestType);

        // not ajax requests, do not need to check. For mvc
        if (ObjectUtils.isEmpty(requestType)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        // ajax requests, but the uri should be excluded
        if (excludePathArrays != null && excludePathArrays.length > 0 && ! ObjectUtils.isEmpty(uri)) {
            for (String path: excludePathArrays) {
                if (uri.contains(path)) {
LOGGER.info("!!!! ");
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }
        }

        String ticket = httpServletRequest.getHeader(ServiceProperties.DEFAULT_CAS_ARTIFACT_PARAMETER);
LOGGER.info("---- ajax doFilter ticket: " + ticket);

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();

        String sessionAssertionTicket = authentication == null ? null : authentication.getCredentials().toString();

        if (securityContext.getAuthentication() != null && securityContext.getAuthentication().isAuthenticated()) {
            // there is authentication info in session
            httpServletResponse.setHeader(ServiceProperties.DEFAULT_CAS_ARTIFACT_PARAMETER, sessionAssertionTicket);
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        // return invalid ticket flag
        httpServletResponse.setHeader(ServiceProperties.DEFAULT_CAS_ARTIFACT_PARAMETER, INVALID_TICKET);
        return;
    }

    @Override
    public void destroy() {
    }




    public String getExcludePaths() {
        return excludePaths;
    }

    public void setExcludePaths(String excludePaths) {
        this.excludePaths = excludePaths;
    }

    public Ehcache getCache() {
        return cache;
    }

    public void setCache(Ehcache cache) {
        this.cache = cache;
    }

    public String[] getExcludePathArrays() {
        return excludePathArrays;
    }

    public void setExcludePathArrays(String[] excludePathArrays) {
        this.excludePathArrays = excludePathArrays;
    }
}
