package com.example.kotlinexer.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TicketAuthenticationFilter extends CasAuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TicketAuthenticationFilter.class);

    public TicketAuthenticationFilter() {
        // set an endpoint for the filter
        super.setRequiresAuthenticationRequestMatcher(
            new AntPathRequestMatcher("/authenticate/ticketValidator", "GET")
        );
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException, IOException {

        LOGGER.info("---- TicketAuthenticationFilter attemptAuthentication");

        final String username = CAS_STATELESS_IDENTIFIER;

        // get ticket
        String password = obtainArtifact(request);
        // get redirection URL
        String service = obtainService(request);
        if (password == null) {
            LOGGER.debug("Fail to get the ticket");
            password = "";
        }
        final TicketCodeAuthenticationToken authRequest = new TicketCodeAuthenticationToken(username, password, service);
        authRequest.setDetails(this.buildDetails(service));

        LOGGER.info("---- TicketAuthenticationFilter attemptAuthentication username:" + username);
        LOGGER.info("---- TicketAuthenticationFilter attemptAuthentication password:" + password);
        LOGGER.info("---- TicketAuthenticationFilter attemptAuthentication service:" + service);

        if (this.getAuthenticationManager() == null) {
            LOGGER.info("---- TicketAuthenticationFilter attemptAuthentication this.getAuthenticationManager() is null");
        }

        return this.getAuthenticationManager().authenticate(authRequest);
    }

    public TicketServiceAuthenticationDetails buildDetails(String service) {
        return new TicketServiceAuthenticationDetails(service);
    }

    protected String obtainService(HttpServletRequest request) {
        return request.getParameter(ServiceProperties.DEFAULT_CAS_SERVICE_PARAMETER);
    }
}
