package com.example.kotlinexer.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.cas.web.authentication.ServiceAuthenticationDetails;

public class TicketServiceAuthenticationDetails implements ServiceAuthenticationDetails {

    private static final Logger LOGGER = LoggerFactory.getLogger(TicketServiceAuthenticationDetails.class);

    private String serviceURL;

    public TicketServiceAuthenticationDetails(String serviceURL) {
        super();
        this.serviceURL = serviceURL;
    }

    @Override
    public String getServiceUrl() {
        return serviceURL;
    }
}
