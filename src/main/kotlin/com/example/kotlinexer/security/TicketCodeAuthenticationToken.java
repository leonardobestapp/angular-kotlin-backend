package com.example.kotlinexer.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.Collection;

public class TicketCodeAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private final Object principal;
    private Object credentials;
    private String authenticationUrl;

    public TicketCodeAuthenticationToken(Object principal, Object credentials, String authenticationUrl) {
        super(principal, credentials);
        this.principal = principal;
        this.credentials = credentials;
        this.authenticationUrl = authenticationUrl;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    public void setCredentials(Object credentials) {
        this.credentials = credentials;
    }

    public String getAuthenticationUrl() {
        return authenticationUrl;
    }

    public void setAuthenticationUrl(String authenticationUrl) {
        this.authenticationUrl = authenticationUrl;
    }
}
