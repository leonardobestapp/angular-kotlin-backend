package com.example.kotlinexer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinexerApplication

fun main(args: Array<String>) {
	runApplication<KotlinexerApplication>(*args)
}
