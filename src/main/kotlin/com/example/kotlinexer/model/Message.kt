package com.example.kotlinexer.model

data class Message(val id: String?, val text: String)
